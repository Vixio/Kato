(function() {
    window.CatGame = function() {
        this._init = function() {
            this.scene = new THREE.Scene();
            this.camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
            this.camera.position.z = 5;

            this.renderer = new THREE.WebGLRenderer();
            this.renderer.setSize( window.innerWidth, window.innerHeight );
            document.body.appendChild( this.renderer.domElement );
            
            this.clock = new THREE.Clock();

            this.trackballControls = new THREE.TrackballControls( this.camera );
            this.trackballControls.rotateSpeed = 1.0;
            this.trackballControls.zoomSpeed   = 1.0;
            this.trackballControls.panSpeed    = 1.0;

            this.raycaster = new THREE.Raycaster();
            this.mouse     = new THREE.Vector2();
            document.addEventListener( 'mousedown', this.onDocumentMouseDown.bind( this ), false );

            this.board = new Board( this.scene );
            this.currentHideFace = this.board.getCubeLen();
        };

        this.onDocumentMouseDown = function( event ) {
            event.preventDefault();

            if ( this.board.isGameOver() ) {
                return;
            }

            this.mouse.x = ( event.clientX / this.renderer.domElement.width ) * 2 - 1;
            this.mouse.y = -( event.clientY / this.renderer.domElement.height ) * 2 + 1;

            this.raycaster.setFromCamera( this.mouse, this.camera );
            var intersects = this.raycaster.intersectObjects( this.board.getCubesMesh() );

            if ( intersects.length > 0 ) {
                if ( intersects[0].object.slot.getStatus() === SlotStatus.EMPTY ) {
                    var obj = intersects[0].object;
                    this.board.putMark( obj.position.z + 1, obj.position.y + 1, obj.position.x + 1 );

                    if ( this.board.getWinner() ) {
                        var winner = this.board.getWinner();
                        setTimeout(function() {
                            if ( winner == SlotStatus.X ) {
                                window.alert("Ha ganado el jugador X");
                            } else {
                                window.alert("Ha ganado el jugador O");
                            }
                        }, 200)
                    }
                }
            }
        }

        this.update = function() {
            var delta = this.clock.getDelta();

            this.trackballControls.update( delta );
        };

        this.render = function() {
            this.update();

            this.renderer.setClearColor(0xEFEFEF, 1);

            requestAnimationFrame( this.render.bind(this) );
            this.renderer.render( this.scene, this.camera );
        }

        this.hideNextFace = function() {
            this.currentHideFace--;
            this.board.hideFace( this.currentHideFace );

            if ( this.currentHideFace < 0 ) {
                this.currentHideFace = this.board.getCubeLen();
            }
        };

        this._init();
        this.render();
    };

    window.game = new CatGame();
})();
