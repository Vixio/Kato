(function(){
    window.Board = function( scene ) {
        this._init = function() {
            this.scene = scene;
            this.foundWinner = false;
            this.data = [];

            this.len = 3;
            for ( var z = 0; z < this.len; z++ ) {
                this.data[z] = [];
                for ( var i = 0; i < this.len; i++ ) {
                    this.data[z][i] = [];
                    for ( var j = 0; j < this.len; j++ ) {
                        this.data[z][i][j] = new Slot( this.scene, i - 1, j - 1, z - 1 );
                    }
                }
            }
        };

        this.getCubeLen = function() {
            return this.len;
        };

        this.getCubesMesh = function() {
            var cubes = [];

            for ( var z = 0; z < this.data.length; z++ ) {
                for ( var  i = 0; i < this.data[z].length; i++ ) {
                    for ( var j = 0; j < this.data[z][i].length; j++ ) {
                        if ( this.data[z][i][j].getCubeMesh().material.transparent ) {
                            continue;
                        }

                        cubes.push( this.data[z][i][j].getCubeMesh() );
                    }
                }
            }

            return cubes;
        };

        this.reset = function() {
            for ( var z = 0; z < this.data.length; z++ ) {
                for ( var i = 0; i < this.data[z].length; i++ ) {
                    for ( var j = 0; j < this.data[z][i].length; j++ ) {
                        this.data[z][i][j].setStatus( SlotStatus.EMPTY );
                    }
                }
            }
        };

        this.isGameOver = function() {
            return this.foundWinner != false;
        };

        this.hideFace = function( hz ) {
            for ( var z = 0; z < this.data.length; z++ ) {
                for ( var i = 0; i < this.data[z].length; i++ ) {
                    for ( var j = 0; j < this.data[z][i].length; j++ ) {
                        if ( z == hz ) {
                            this.data[z][i][j].getCubeMesh().material.transparent = true;
                            this.data[z][i][j].getCubeMesh().material.opacity = 0.1;
                        } else {
                            this.data[z][i][j].getCubeMesh().material.transparent = false;
                            this.data[z][i][j].getCubeMesh().material.opacity = 1;
                        }
                        this.data[z][i][j].getCubeMesh().material.needsUpdate = true;
                    }
                }
            }
        };

        this._lookForWinnner = function( z, i, j ) {
            var status = this.data[z][i][j].getStatus();
            var len = this.data.length - 1;

            var intersects = [
                { z: z, i: i, j: 0, dz: 0, di: 0, dj: 1 }
                , { z: z, i: 0, j: j, dz: 0, di: 1, dj: 0 }
                , { z: 0, i: i, j: j, dz: 1, di: 0, dj: 0 }
                , { z: len, i: 0, j: j, dz: -1, di: 1, dj: 0 }
                , { z: len, i: len, j: j, dz: -1, di: -1, dj: 0 }
                , { z: len, i: i, j: len, dz: -1, di: 0, dj: -1 }
                , { z: len, i: i, j: 0, dz: -1, di: 0, dj: 1 }
                , { z: z, i: len, j: 0, dz: 0, di: -1, dj: 1 }
                , { z: z, i: len, j: len, dz: 0, di: -1, dj: -1 }
                , { z: len, i: 0, j: len, dz: -1, di: 1, dj: -1 }
                , { z: len, i: len, j: len, dz: -1, di: -1, dj: -1 }
                , { z: 0, i: len, j: len, dz: 1, di: -1, dj: -1 }
                , { z: 0, i: 0, j: len, dz: 1, di: 1, dj: -1 }
            ];

            for ( var i = 0; i < intersects.length; i++ ) {
                var inter = intersects[i];
                var count = this._countMarks( status, inter.z, inter.i, inter.j, inter.dz, inter.di, inter.dj );
                if ( count === this.data.length ) {
                    return true;
                }
            }

            return false;
        };

        this._countMarks = function( mark, cz, ci, cj, dz, di, dj ) {
            var count = 0;

            while( cz >= 0 && cz < this.data.length && ci >= 0 && ci < this.data[cz].length && cj >= 0 && cj < this.data[cz][ci].length ) {
                if ( this.data[cz][ci][cj].getStatus() === mark ) {
                    count++;
                }

                cz += dz;
                ci += di;
                cj += dj;
            }

            return count;
        };

        this.putMark = function( z, j, i ) {
            if ( this.foundWinner ) {
                return;
            }

            this._invertMark();

            this.data[z][i][j].setStatus( this.currentMark );
            if ( this._lookForWinnner( z, i, j ) ) {
                this.foundWinner = this.data[z][i][j].getStatus();
            }
        };

        this._invertMark = function() {
            if ( this.currentMark === SlotStatus.X ) {
                this.currentMark = SlotStatus.O;
            } else {
                this.currentMark = SlotStatus.X;
            }
        };

        this.getWinner = function() {
            return this.foundWinner;
        };

        this._init();
    };
})();


