(function() {
    window.SlotStatus = {
        EMPTY: 0,
        X: 1,
        O: 2,
    };

    window.Slot = function(scene, x, y, z) {
        this._init = function() {
            var geometry = new THREE.BoxGeometry( 1, 1, 1 );
            var material = new THREE.MeshBasicMaterial( { map: this._getTextureForStatus( SlotStatus.EMPTY ) } );

            this.cube = new THREE.Mesh( geometry, material );
            this.cube.position.x = x;
            this.cube.position.y = y;
            this.cube.position.z = z;
            scene.add( this.cube );

            this.setStatus( SlotStatus.EMPTY );
            this.cube.slot = this;
        };

        this._getTextureForStatus = function(status) {
            var lookupTexture = {
                0: '/images/empty_slot.png', // Empty
                1: '/images/x_slot.png', // X
                2: '/images/o_slot.png'  // O
            };

            return THREE.ImageUtils.loadTexture( lookupTexture[status] );
        };

        this.setStatus = function(status) {
            this.status = status;
            this.cube.material.map = this._getTextureForStatus( status );
            this.cube.material.needsUpdate = true;
        };

        this.getStatus = function() {
            return this.status;
        };

        this.getCubeMesh = function() {
            return this.cube;
        };

        this._init();
    };
})();
