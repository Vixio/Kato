Dependencias
----
- NodeJS (https://nodejs.org/en/)

Ejecutar el proyecto
---

Una vez instalado NodeJS puedes correr el proyecto ejecutando con los siguientes
comandos en el directorio del proyecto.
~~~
$ npm install
$ npm start
~~~

Si todo fuer correcto, ahora deberías poder ver el proyecto en http://localhost:3000


Dónde esta el código?
----
Los archivos con la lógica del juego se encuentran dentro de la carpeta
**public/javascript**.

- Slot.js
- Board.js
- Game.js
